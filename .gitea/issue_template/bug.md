---
name: "Bug Report"
about: "Use this template when reporting a bug, so you don't forget important information we'd ask for later."
title: "Bug: "
labels: 
- kind/bug
---

### describe your environment
- tea version used (`tea -v`):
  - [ ] I also reproduced the issue [with the latest master build](https://dl.gitea.io/tea/master)
- Gitea version used:
  - [ ] the issue only occured after updating gitea recently
- operating system:

- I make use of...
  - [ ] ssh_agent
  - [ ] .ssh/config or .gitconfig host aliases
  - [ ] non-standard ports for gitea and/or ssh
  - [ ] something else that's likely to interact badly with tea: ...

- please provide the output of `git remote -v` (if the issue is related to tea not finding resources on Gitea)
    ```

    ```

### describe the issue (observed vs expected behaviour)


